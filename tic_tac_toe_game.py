import pygame
import sys

class TicTacToe():
    def __init__(self):
        self.screen_size = (400, 400)
        self.game_running = True
        pygame.init()
        self.game_window = pygame.display.set_mode(self.screen_size)
        pygame.display.set_caption("TIC TAC TOE")
        self.clock = pygame.time.Clock()
        self.change_player = False

    def draw_lines(self, game_window, screen_size):
        vertical_line_1 = int(screen_size[0] / 3)
        pygame.draw.line(game_window, (0, 0, 0), (vertical_line_1, 0), (vertical_line_1, screen_size[0]), 4)
        vertical_line_2 = vertical_line_1 * 2
        pygame.draw.line(game_window, (0, 0, 0), (vertical_line_2, 0), (vertical_line_2, screen_size[0]), 4)

        horizontal_line_1 = int(screen_size[1] / 3)
        pygame.draw.line(game_window, (0, 0, 0), (0, horizontal_line_1), (screen_size[0], horizontal_line_1), 4)
        horizontal_line_2 = horizontal_line_1 * 2
        pygame.draw.line(game_window, (0, 0, 0), (0, horizontal_line_2), (screen_size[0], horizontal_line_2), 4)

    def initialise_board(self, screen_size):
        board = [[] for i in range(3)]
        counter = 1
        rect_width = int(screen_size[0] / 3)
        rect_height = int(screen_size[1] / 3)
        top = 0
        for i in range(3):
            left = 0
            for j in range(3):
                board[i].append({
                    'played': False,
                    'player': str(counter),
                    'rect': pygame.Rect(left, top, rect_width, rect_height)
                })
                left += rect_width
                counter += 1
            top += rect_height
        return board
    
    def winner_check(self, board, player):
        for row in board:
            if (row[0]['player'] == player and row[1]['player'] == player and
                    row[2]['player'] == player):
                return True
        for i in range(3):
            if (board[0][i]['player'] == player and board[1][i]['player'] == player and
                board[2][i]['player'] == player):
                return True

        if (board[0][0]['player'] == player and board[1][1]['player'] == player and
            board[2][2]['player'] == player):
            return True

        if (board[0][2]['player'] == player and board[1][1]['player'] == player and
            board[2][0]['player'] == player):
            return True

        return False
    
    def draw_check(self, board):
        return all([all([r['played'] for r in row]) for row in board])

    def display_player(self, letter, color, rectangle):
        font = pygame.font.SysFont('comicsansms', 150, bold=True)
        text = font.render(letter, True, color)
        selected_rectangle = text.get_rect(center=rectangle.center)
        self.game_window.blit(text, selected_rectangle)

    def end_game_message(self, message, instruction, color):
        match_font = pygame.font.SysFont('comicsansms', 80)
        match_text = match_font.render(message, True, color)
        match_rect = match_text.get_rect(center=(self.screen_size[0]/2, self.screen_size[1]/2))
        self.game_window.blit(match_text, match_rect)
        
        inst_font = pygame.font.SysFont('comicsansms', 25)
        inst_text = inst_font.render(instruction, True, color)
        inst_rect = inst_text.get_rect(center=(self.screen_size[0]/2, match_rect.bottom + 30))
        self.game_window.blit(inst_text, inst_rect)
        
    def on_event(self, event, game):
        if event.type == pygame.QUIT:
            self.game_running = False
        if (event.type == pygame.MOUSEBUTTONUP and event.button == 1 
           and not (game['win'] or game['draw'])):
            position = pygame.mouse.get_pos()
            self.change_player = self.on_loop(game, position)
            game['win'] = self.winner_check(game['board'], game['player'])
            game['draw'] = self.draw_check(game['board'])
            # if not (game['win'] or game['draw']):
            game['change_player'] = self.change_player
            self.change_player_func(game)
    
    def change_player_func(self, game):
        if game['change_player'] and not game['win'] or game['draw']:
            if game['player'] == 'X':
                game['player'] = 'O'
            else:
                game['player'] = 'X'

    def update(self, game, p1, p2):
        game['board'][p1][p2]['played'] = True
        game['board'][p1][p2]['player'] = game['player']
        
        return True

    def on_loop(self, game, position):
        someone_played = False
        if (0 <= position[0] < 133
            and 0 <= position[1] < 133 
            and not game['board'][0][0]['played']): 
            someone_played = self.update(game, 0, 0)
        elif (133 <= position[0] < 266
            and 0 <= position[1] < 133 
            and not game['board'][0][1]['played']): 
            someone_played = self.update(game, 0, 1)
        elif (position[0] >= 266
            and 0 <= position[1] < 133
            and not game['board'][0][2]['played']): 
            someone_played = self.update(game, 0, 2)
        elif ( 0 <= position[0] < 133
            and 133 <= position[1] < 266 
            and not game['board'][1][0]['played']): 
            someone_played = self.update(game, 1, 0)
        elif ( 133 <= position[0] < 266
            and 133 <= position[1] < 266 
            and not game['board'][1][1]['played']): 
            someone_played = self.update(game, 1, 1)
        elif ( position[0] >= 266
            and 133 <= position[1] < 266 
            and not game['board'][1][2]['played']): 
            someone_played = self.update(game, 1, 2)
        elif ( 0 <= position[0] < 133
            and position[1] >= 266  
            and not game['board'][2][0]['played']): 
            someone_played = self.update(game, 2, 0)
        elif ( 133 <= position[0] < 266
            and position[1] >= 266 
            and not game['board'][2][1]['played']): 
            someone_played = self.update(game, 2, 1)
        elif (position[0] >= 266
            and position[1] >= 266 
            and not game['board'][2][2]['played']): 
            someone_played = self.update(game, 2, 2)

        return someone_played

    def on_render(self, game):
        if not (game['win'] or game['draw']):
            self.game_window.fill((200, 200, 200))
        else:
            self.game_window.fill((10, 10, 10))

        self.draw_lines(self.game_window, self.screen_size)

        for row in game['board']:
            for column in row:
                if column['player'] == 'X':
                    self.display_player('X', (125, 60, 60), column['rect'])
                elif column['player'] == 'O':
                    self.display_player('O', (20, 125, 20), column['rect'])

        if game['win']:
            message = 'Player ' + game['player'] + ' won!'
            self.end_game_message(message, "Press Space or Delete to play again", (255, 255, 255))
        elif not game['win'] and game['draw']:
            message = 'Draw Match'
            self.end_game_message(message, "Press Space or Delete to play again", (255, 255, 255))

        pygame.display.update()
        self.clock.tick(60)


    def on_cleanup(self):
        pygame.quit()
        sys.exit()

    def initialise(self):
        return {
                'board': self.initialise_board(self.screen_size),
                'player': 'X',
                'win': False,
                'draw': False,
                'change_player': False
                }
    
    def on_execute(self):

        game = self.initialise()

        while (self.game_running):
            for event in pygame.event.get():
                self.on_event(event, game)
            key_pressed = pygame.key.get_pressed()            
            if (game['win'] or game['draw']) and (key_pressed[pygame.K_SPACE] or key_pressed                    [pygame.K_DELETE]):
                game = self.initialise()
            self.on_render(game)
        
        self.on_cleanup()

if __name__ == "__main__":
    tictactoe = TicTacToe()
    tictactoe.on_execute()